package junit;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

public class FizzBuzzTest{


		private FizzBuzz fizzbuzz;

		@Before
		public void インスタンス生成(){
			fizzbuzz = new FizzBuzz();
		}

		@Test
		public void 引数に1を与えたら1を返す(){
			assertEquals("1",fizzbuzz.response(1));
		}
		@Test
		public void 引数3を与えたらFizzを返す(){
			assertEquals("Fizz",fizzbuzz.response(3));
		}
		@Test
		public void 引数5を与えたらBuzzを返す(){
			assertEquals("Buzz",fizzbuzz.response(5));
		}
		@Test
		public void 引数がを与えたらFizzBuzzを返す(){
			assertEquals("FizzBuzz",fizzbuzz.response(15));
		}
		@Test
		public void 引数が1を与えたら1を返す(){
			assertEquals("1",fizzbuzz.response(1));
		}
		@Test
		public void 引数100を超えたらBuzzを返す(){
			assertEquals("Buzz",fizzbuzz.response(100));

		}
		@Test(expected = IndexOutOfBoundsException.class)
		public void 引数0を与えたらエラーとなる(){
			fizzbuzz.response(0);
		}
		@Test(expected = IndexOutOfBoundsException.class)
		public void 引数101を与えたらエラーとなる(){
			fizzbuzz.response(101);
		}
}


